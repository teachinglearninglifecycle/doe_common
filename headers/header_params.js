"use strict";
exports.__esModule = true;
exports.getDoETllHeaderParameters = void 0;
exports.getDoETllHeaderParameters = function (params) {
    var application = params.headers['application'];
    var version = params.headers['version'];
    if (!application) {
        return null;
    }
    if (!version) {
        return null;
    }
    var doETllHeaderParams = {
        application: application,
        version: version
    };
    if (params.headers['collection']) {
        doETllHeaderParams.collection = params.headers['collection'];
    }
    if (params.headers['event']) {
        doETllHeaderParams.event = params.headers['event'];
    }
    return doETllHeaderParams;
};
