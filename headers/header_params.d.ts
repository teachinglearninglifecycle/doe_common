import * as express from 'express';
export interface DoETllHeaderParams {
    application: string;
    version: string;
    collection?: string;
    event?: string;
}
export declare const getDoETllHeaderParameters: (params: express.Request) => DoETllHeaderParams;
//# sourceMappingURL=header_params.d.ts.map