import * as express from 'express';
import { ParamsDictionary } from 'express-serve-static-core';

export interface DoETllHeaderParams {
    application: string;
    version: string;
    collection?: string;
    event?: string;
}

export const getDoETllHeaderParameters = function(params: express.Request): DoETllHeaderParams{

    const application: string = params.headers['application'] as string;
    const version: string = params.headers['version'] as string;

    if(!application) {
        return null as unknown as DoETllHeaderParams;
    }

    if(!version) {
        return null as unknown as DoETllHeaderParams;
    }

    const doETllHeaderParams: DoETllHeaderParams = {
        application,
        version
    };

    if(params.headers['collection']) {
        doETllHeaderParams.collection = params.headers['collection'] as string;
    }

    if(params.headers['event']) {
        doETllHeaderParams.event = params.headers['event'] as string;
    }

    return doETllHeaderParams;

}