import express = require("express");
export interface SearchFilterParams {
    fieldName: string;
    fieldValue: any;
    operator: string;
}
export interface SearchParamsObject {
    queryString?: string;
    from?: number;
    limit?: number;
    sort?: string[];
    searchFilterParams?: SearchFilterParams[];
}
export interface SearchResponseObject {
    start: number;
    end: number;
    recordCount: number;
    documents: any[];
    error?: any;
}
export declare enum SearchQueryParamsItems {
    QUERY = "query",
    FROM = "from",
    LIMIT = "limit",
    SORT = "sort",
    SEARCH_FILTER_PARAMS = "filter"
}
export declare class SearchParams {
    static getSearchParams(req: express.Request): SearchParamsObject;
}
//# sourceMappingURL=search_params.d.ts.map