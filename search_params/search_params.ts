import express = require("express");

export interface SearchFilterParams {

    fieldName: string;
    fieldValue: any;
    operator: string;

}

export interface SearchParamsObject {
    queryString?: string;
    from?: number;
    limit?: number;
    sort?: string[];
    searchFilterParams?: SearchFilterParams[];
}

export interface SearchResponseObject {
    start: number;
    end: number;
    recordCount: number;
    documents: any[];
    error?: any;
}

export enum SearchQueryParamsItems {
    QUERY = "query",
    FROM = "from",
    LIMIT = "limit",
    SORT = "sort",
    SEARCH_FILTER_PARAMS = "filter"
}

export class SearchParams {

    static getSearchParams(req: express.Request): SearchParamsObject {

        const searchObject: SearchParamsObject = {};

        if (req.body[SearchQueryParamsItems.QUERY]) searchObject.queryString = req.body[SearchQueryParamsItems.QUERY] as string;

        if(req.body[SearchQueryParamsItems.SORT]) searchObject.sort = req.body[SearchQueryParamsItems.SORT] as string[];

        if (req.body[SearchQueryParamsItems.FROM]) {
            try{
              searchObject.from = parseInt(req.body[SearchQueryParamsItems.FROM] as string);
            } catch(e) {
                return null as unknown  as SearchParamsObject;
            }
        }

        if(req.body[SearchQueryParamsItems.LIMIT]) {
            try{
                searchObject.limit = parseInt(req.body[SearchQueryParamsItems.LIMIT] as string);
            } catch (e) {
                return null as unknown as SearchParamsObject;
            }
        }

        if(req.body[SearchQueryParamsItems.SEARCH_FILTER_PARAMS]) {
            searchObject.searchFilterParams = req.body[SearchQueryParamsItems.SEARCH_FILTER_PARAMS];
        }

        return searchObject;
    }

}

