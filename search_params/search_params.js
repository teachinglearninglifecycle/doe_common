"use strict";
exports.__esModule = true;
exports.SearchParams = exports.SearchQueryParamsItems = void 0;
var SearchQueryParamsItems;
(function (SearchQueryParamsItems) {
    SearchQueryParamsItems["QUERY"] = "query";
    SearchQueryParamsItems["FROM"] = "from";
    SearchQueryParamsItems["LIMIT"] = "limit";
    SearchQueryParamsItems["SORT"] = "sort";
    SearchQueryParamsItems["SEARCH_FILTER_PARAMS"] = "filter";
})(SearchQueryParamsItems = exports.SearchQueryParamsItems || (exports.SearchQueryParamsItems = {}));
var SearchParams = /** @class */ (function () {
    function SearchParams() {
    }
    SearchParams.getSearchParams = function (req) {
        var searchObject = {};
        if (req.body[SearchQueryParamsItems.QUERY])
            searchObject.queryString = req.body[SearchQueryParamsItems.QUERY];
        if (req.body[SearchQueryParamsItems.SORT])
            searchObject.sort = req.body[SearchQueryParamsItems.SORT];
        if (req.body[SearchQueryParamsItems.FROM]) {
            try {
                searchObject.from = parseInt(req.body[SearchQueryParamsItems.FROM]);
            }
            catch (e) {
                return null;
            }
        }
        if (req.body[SearchQueryParamsItems.LIMIT]) {
            try {
                searchObject.limit = parseInt(req.body[SearchQueryParamsItems.LIMIT]);
            }
            catch (e) {
                return null;
            }
        }
        if (req.body[SearchQueryParamsItems.SEARCH_FILTER_PARAMS]) {
            searchObject.searchFilterParams = req.body[SearchQueryParamsItems.SEARCH_FILTER_PARAMS];
        }
        return searchObject;
    };
    return SearchParams;
}());
exports.SearchParams = SearchParams;
