import * as express from 'express';
import * as _moment from 'moment';
import {DoETllHeaderParams, getDoETllHeaderParameters} from "../headers/header_params";

export interface DataObject {
    application: string;
    collection: string;
    rawCollection?: string;
    data?: any;
    id?: string;
    originalId?: string;
    version?: number;
    created?: string;
    modified?: string;
    archive?: any[];
    event?: string;
}

export interface FileObject {
    FileName: string;
    Encoding: string;
    MimeType: string;
    CreatedDate?: string;
    ModifiedDate?: string;
    objFileData?: number[];
    Key?: string;
    Etag?: string;
    DownloadUrl?: string;
    Created?: boolean;
    ContentEncoding?: string;
    ContentLength?: string;
    VersionId?: string;
    VersionNumber: number;
    PreviousVersions?: PreviousFileObject[];
    ContentType?: string;
    FileLocation?: string;
}

export interface PreviousFileObject {
    VersionId: string;
    VersionNumber: number;
    CreatedDate: string;
    ReplacedDate?: string;
}

export class DoeDataObject {

    static getDataObject(req: express.Request, res: express.Response): DataObject {

        const headers: DoETllHeaderParams = getDoETllHeaderParameters(req);

        if(!headers){
            res.status(400).jsonp({
                msg: 'Invalid Header'
            });
            return null as unknown as DataObject
        }

        const dataObject: DataObject =   {
            application: headers.application,
            rawCollection: headers.collection || null as unknown as string,
            collection: headers.collection ? (`${headers.application}` + `_${headers.collection}`): null as unknown as string,
            version: headers.version ? parseInt(headers.version) : 1
        };

        if(headers.event) {
            dataObject.event = headers.event;
        }

        if(req.body){
            dataObject.data = req.body;
        }

        if(req.params.id){
            dataObject.id = req.params.id;
        }

        if(req?.body?.id){
           dataObject.id = req.body.id;
        }

        if(req?.body?.record?.id){
            dataObject.id = req.body.record.id;
        }

        return dataObject;

    }

    static initNewDataObject(req: express.Request, res: express.Response): DataObject  {
        const dataObject: DataObject = this.getDataObject(req, res);

        if(dataObject == null) {
            return null as unknown as DataObject;
        }

        dataObject.version = dataObject.version || 1;
        dataObject.created = _moment.utc().format();
        dataObject.modified = _moment.utc().format();

        return dataObject;
    }

}