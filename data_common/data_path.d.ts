import * as express from 'express';
export interface DataObject {
    application: string;
    collection: string;
    rawCollection?: string;
    data?: any;
    id?: string;
    originalId?: string;
    version?: number;
    created?: string;
    modified?: string;
    archive?: any[];
    event?: string;
}
export interface FileObject {
    FileName: string;
    Encoding: string;
    MimeType: string;
    CreatedDate?: string;
    ModifiedDate?: string;
    objFileData?: number[];
    Key?: string;
    Etag?: string;
    DownloadUrl?: string;
    Created?: boolean;
    ContentEncoding?: string;
    ContentLength?: string;
    VersionId?: string;
    VersionNumber: number;
    PreviousVersions?: PreviousFileObject[];
    ContentType?: string;
    FileLocation?: string;
}
export interface PreviousFileObject {
    VersionId: string;
    VersionNumber: number;
    CreatedDate: string;
    ReplacedDate?: string;
}
export declare class DoeDataObject {
    static getDataObject(req: express.Request, res: express.Response): DataObject;
    static initNewDataObject(req: express.Request, res: express.Response): DataObject;
}
//# sourceMappingURL=data_path.d.ts.map