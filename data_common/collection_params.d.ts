export declare enum CollectionMappingTypes {
    ARRAY = "array",
    BOOLEAN = "boolean",
    BYTE = "byte",
    DATE = "date",
    DOUBLE = "double",
    FLOAT = "float",
    GEO_POINT = "geo_point",
    GEO_SHAPE = "geo_shape",
    INTEGER = "integer",
    LONG = "long",
    OBJECT = "object",
    POINT = "point",
    SHAPE = "shape",
    STRING = "string",
    TEXT = "text"
}
//# sourceMappingURL=collection_params.d.ts.map