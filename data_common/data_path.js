"use strict";
exports.__esModule = true;
exports.DoeDataObject = void 0;
var _moment = require("moment");
var header_params_1 = require("../headers/header_params");
var DoeDataObject = /** @class */ (function () {
    function DoeDataObject() {
    }
    DoeDataObject.getDataObject = function (req, res) {
        var _a, _b, _c;
        var headers = header_params_1.getDoETllHeaderParameters(req);
        if (!headers) {
            res.status(400).jsonp({
                msg: 'Invalid Header'
            });
            return null;
        }
        var dataObject = {
            application: headers.application,
            rawCollection: headers.collection || null,
            collection: headers.collection ? ("" + headers.application + ("_" + headers.collection)) : null,
            version: headers.version ? parseInt(headers.version) : 1
        };
        if (headers.event) {
            dataObject.event = headers.event;
        }
        if (req.body) {
            dataObject.data = req.body;
        }
        if (req.params.id) {
            dataObject.id = req.params.id;
        }
        if ((_a = req === null || req === void 0 ? void 0 : req.body) === null || _a === void 0 ? void 0 : _a.id) {
            dataObject.id = req.body.id;
        }
        if ((_c = (_b = req === null || req === void 0 ? void 0 : req.body) === null || _b === void 0 ? void 0 : _b.record) === null || _c === void 0 ? void 0 : _c.id) {
            dataObject.id = req.body.record.id;
        }
        return dataObject;
    };
    DoeDataObject.initNewDataObject = function (req, res) {
        var dataObject = this.getDataObject(req, res);
        if (dataObject == null) {
            return null;
        }
        dataObject.version = dataObject.version || 1;
        dataObject.created = _moment.utc().format();
        dataObject.modified = _moment.utc().format();
        return dataObject;
    };
    return DoeDataObject;
}());
exports.DoeDataObject = DoeDataObject;
