import axios, {AxiosRequestConfig, AxiosResponse} from 'axios'
import {DataObject, FileObject} from "../data_path";
import {Request, Response} from "express";
import {FileStoreRequests} from "./file_store_requests";
import {DataStoreRequests} from "./data_store_requests";

export interface ApiRequestObject {
    application: string;
    version: number;
    collection?: string;
    event?: string;
}

export class ApiRequests {

    config: ApiRequestObject;



    constructor(config: ApiRequestObject) {
        this.config = config;
    }

    static getApiConfig(dataObject: DataObject, baseConfig: ApiRequestObject): ApiRequestObject {
        const apiConfig: ApiRequestObject = {
            application: baseConfig.application || dataObject.application,
            version: baseConfig?.version,
            collection: dataObject.rawCollection,
        }

        if(dataObject.version) {
            apiConfig.version = dataObject.version;
        }

        if(dataObject.event) {
            apiConfig.event = dataObject.event;
        }

        return apiConfig;
    }

    doRequest(apiRequestObject: AxiosRequestConfig, dataObject?: DataObject): Promise<any> {

        if (!apiRequestObject?.url) {
            return null as unknown as any;
        }

        if( !apiRequestObject.headers) {
            apiRequestObject.headers = {};
        }

        if(!dataObject) {
            apiRequestObject.headers['application'] = this.config.application;
            apiRequestObject.headers['version'] = this.config.version;

            if (this.config.collection) apiRequestObject.headers['collection'] = this.config.collection;

            if (this.config.event) apiRequestObject.headers['event'] = this.config.collection;
        }

        if(dataObject) {
            apiRequestObject.headers['application'] = dataObject.application;
            apiRequestObject.headers['version'] = dataObject.version;
            if(dataObject.rawCollection) {
                apiRequestObject.headers['collection'] = dataObject.rawCollection;
            }

            if(dataObject.event) {
                apiRequestObject.headers['event'] = dataObject.event;
            }
        }

        apiRequestObject.maxBodyLength = Infinity;
        apiRequestObject.maxBodyLength = Infinity;
        //apiRequestObject.headers['Content-Type'] = "Content-Type: multipart/form-data;boundary=" + apiRequestObject.data.getBoundary();

        return axios.request(apiRequestObject)
            .then((result: AxiosResponse<any>) => {
                return result.data
            }).catch(reason => {
                console.log('API REQUEST ERROR', apiRequestObject, reason, dataObject);
                throw Error(reason);
        });

    }

    async processDataAndFiles(dataObject: DataObject, fields: any, files: any, dataStoreRequest : DataStoreRequests,fileStoreRequest: FileStoreRequests): Promise< any>{

        const dataStoreSaveResult: any = await dataStoreRequest.createRequest(dataObject, fields);

        if (!dataStoreSaveResult?.record?.id) {
            return {
                rs_error: 'unable to save resource',
            };
        }

        dataObject.id = dataStoreSaveResult.record.id;

        const fileList= await this.processFiles(dataObject, files, fileStoreRequest);


        return await dataStoreRequest.patchRequest(dataObject,{
            ...dataStoreSaveResult,
            files: fileList
        });

    }

    async putDataAndFiles(dataObject: DataObject, fields: any, files: any, dataStoreRequest : DataStoreRequests,fileStoreRequest: FileStoreRequests){

        const fileList= await this.processFiles(dataObject, files, fileStoreRequest);

        const updateObject = {
            ...fields,
            files: fileList
        };

        return await dataStoreRequest.putRequest(dataObject, updateObject);

    }


    async processFiles(dataObject: DataObject, files: any, fileStoreRequest: FileStoreRequests): Promise<FileObject[]>{
        const fileStoreResult = await fileStoreRequest.createRequest(dataObject, files);

        if (fileStoreResult.rs_error) {
            return {
                rs_error: fileStoreResult.rs_error
            } as any;
        }

        const fileList: FileObject[] = [];

        fileStoreResult.forEach((obj: any) => {

            const fileDetails = fileStoreRequest.updateFileObjectFromDataStore(files[obj.fileName] as FileObject, obj);
            delete fileDetails.objFileData;
            fileList.push(fileDetails);

        });

        return fileList;
    }

}