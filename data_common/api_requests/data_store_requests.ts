import { AxiosRequestConfig } from "axios";
import {DataObject} from "../data_path";
import {ApiRequestObject, ApiRequests} from "./api_request";


export class DataStoreRequests {

    private readonly _url: string;
    private _application: string;
    private readonly _version: number;
    private readonly _apiConfig: ApiRequestObject;

    constructor(url: string, application: string, version: string) {
        this._url = url;
        this._application = application;
        this._version = parseInt(version);
        this._apiConfig = {
            version: this._version,
            application: application
        };
    }

    async createRequest(dataObject: DataObject, data: any): Promise<any> {

        const requestObject: AxiosRequestConfig = {
            url: this._url,
            data,
            method: 'POST',
        }

        const apiConfig: ApiRequestObject = ApiRequests.getApiConfig(dataObject, this._apiConfig);

        const apiRequest = new ApiRequests(apiConfig);
        return await apiRequest.doRequest(requestObject)
            .then(result => result)
            .catch(error => {
                return {
                    rs_error: 'Resource Creation Error',
                    description: error
                }
            });
    }

    async patchRequest(dataObject: DataObject, data: any): Promise<any> {

        const requestObject: AxiosRequestConfig = {
          url: this._url + '/' + dataObject.id,
          data,
          method: "PATCH"
        };

        const apiConfig: ApiRequestObject = ApiRequests.getApiConfig(dataObject, this._apiConfig);

        const apiRequest = new ApiRequests(apiConfig);
        return await apiRequest.doRequest(requestObject)
            .then(result => result)
            .catch(error => {
                return {
                    rs_error: 'Resource Patch Error',
                    description: error
                }
            });

    }

    async putRequest(dataObject: DataObject, data: any): Promise<any> {

        const requestObject: AxiosRequestConfig = {
            url: this._url + '/' + dataObject.id,
            data,
            method: "PUT"
        };

        const apiConfig: ApiRequestObject = ApiRequests.getApiConfig(dataObject, this._apiConfig);

        const apiRequest = new ApiRequests(apiConfig);
        return await apiRequest.doRequest(requestObject)
            .then(result => result)
            .catch(error => {
                return {
                    rs_error: 'Resource Patch Error',
                    description: error
                }
            });

    }

    async getRequest(dataObject: DataObject, url: string, api: ApiRequests): Promise<any> {

        const requestConfig: AxiosRequestConfig = {
            url,
            method: "GET"
        };

        return await api.doRequest(requestConfig, dataObject)
            .then(result => {
                return result
            })
            .catch(error => {
                console.log(error);
                throw Error('Unable to read document');
            });

    }

}