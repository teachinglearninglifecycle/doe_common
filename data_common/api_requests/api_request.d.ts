import { AxiosRequestConfig } from 'axios';
import { DataObject, FileObject } from "../data_path";
import { FileStoreRequests } from "./file_store_requests";
import { DataStoreRequests } from "./data_store_requests";
export interface ApiRequestObject {
    application: string;
    version: number;
    collection?: string;
    event?: string;
}
export declare class ApiRequests {
    config: ApiRequestObject;
    constructor(config: ApiRequestObject);
    static getApiConfig(dataObject: DataObject, baseConfig: ApiRequestObject): ApiRequestObject;
    doRequest(apiRequestObject: AxiosRequestConfig, dataObject?: DataObject): Promise<any>;
    processDataAndFiles(dataObject: DataObject, fields: any, files: any, dataStoreRequest: DataStoreRequests, fileStoreRequest: FileStoreRequests): Promise<any>;
    putDataAndFiles(dataObject: DataObject, fields: any, files: any, dataStoreRequest: DataStoreRequests, fileStoreRequest: FileStoreRequests): Promise<any>;
    processFiles(dataObject: DataObject, files: any, fileStoreRequest: FileStoreRequests): Promise<FileObject[]>;
}
//# sourceMappingURL=api_request.d.ts.map