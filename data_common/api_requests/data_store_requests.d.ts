import { DataObject } from "../data_path";
import { ApiRequests } from "./api_request";
export declare class DataStoreRequests {
    private readonly _url;
    private _application;
    private readonly _version;
    private readonly _apiConfig;
    constructor(url: string, application: string, version: string);
    createRequest(dataObject: DataObject, data: any): Promise<any>;
    patchRequest(dataObject: DataObject, data: any): Promise<any>;
    putRequest(dataObject: DataObject, data: any): Promise<any>;
    getRequest(dataObject: DataObject, url: string, api: ApiRequests): Promise<any>;
}
//# sourceMappingURL=data_store_requests.d.ts.map