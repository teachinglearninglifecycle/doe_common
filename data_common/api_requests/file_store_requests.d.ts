import { DataObject, FileObject } from "../data_path";
export declare class FileStoreRequests {
    private readonly _url;
    private _application;
    private readonly _version;
    private readonly _apiConfig;
    constructor(url: string, application: string, version: string);
    createRequest(dataObject: DataObject, files: any): Promise<any>;
    updateFileObjectFromDataStore(fileObject: FileObject, obj: any): FileObject;
}
//# sourceMappingURL=file_store_requests.d.ts.map