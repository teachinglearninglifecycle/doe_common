import {ApiRequestObject, ApiRequests} from "./api_request";
import {DataObject, FileObject} from "../data_path";
const FormData = require("form-data");
import {Readable} from "stream";
import {AxiosRequestConfig} from "axios";

export class FileStoreRequests {
    private readonly _url: string;
    private _application: string;
    private readonly _version: number;
    private readonly _apiConfig: ApiRequestObject;

    constructor(url: string, application: string, version: string) {
        this._url = url;
        this._application = application;
        this._version = parseInt(version);
        this._apiConfig = {
            version: this._version,
            application: application
        };
    }

    async createRequest(dataObject: DataObject, files: any): Promise<any> {

        const url = this._url;
        const formData = new FormData();
        formData.append('id', dataObject.id);

        for (const fileName in files) {
            const fileStream = Buffer.from(files[fileName].objFileData);
            formData.append('file', fileStream, {
                filename: fileName,
                knownLength: files[fileName].length
            });
        }

        const userHeaders: any = {
            application: dataObject.application,
            collection: dataObject.rawCollection,
            version: dataObject.version + ''
        };

        if (dataObject.event) {
            userHeaders['event'] = dataObject.event;
            this._apiConfig.event = dataObject.event;
        } else {
            this._apiConfig.event = null as unknown as string;
        }

        const requestObject: AxiosRequestConfig = {
            url,
            method: 'POST',
            headers: formData.getHeaders(userHeaders),
            data: formData
        };

        const apiConfig: ApiRequestObject = ApiRequests.getApiConfig(dataObject, this._apiConfig);

        const apiRequest = new ApiRequests(apiConfig);
        return await apiRequest.doRequest(requestObject)
            .then(result => result)
            .catch(err => {
                return {
                    rs_error: 'unable to save file',
                    description: err
                };
            });
    }

    updateFileObjectFromDataStore(fileObject: FileObject, obj: any): FileObject {

        fileObject.FileLocation = obj.FileLocation;
        fileObject.Key = obj.Key;
        fileObject.Etag = obj.Etag;
        fileObject.DownloadUrl = obj.DownloadUrl;
        fileObject.Created = obj.CREATED;
        fileObject.VersionId = obj.VersionId;
        fileObject.ContentEncoding = obj.ContentEncoding;
        fileObject.ContentLength = obj.ContentLength;
        fileObject.VersionId = obj.VersionId;
        fileObject.ContentType = obj.ContentType;
        fileObject.PreviousVersions = [{
            CreatedDate: fileObject.CreatedDate as string,
            VersionId: obj.VersionId,
            VersionNumber: fileObject.VersionNumber

        }];

        return fileObject;

    }

}