import { DataObject } from "../data_common/data_path";
import { Request, Response } from "express";
export declare class RequestForwarding {
    static forwardRequest(req: Request, res: Response, dataObject: DataObject, redirect: string): Promise<any>;
}
//# sourceMappingURL=request_forwarding.d.ts.map