import {DataObject} from "../data_common/data_path";
import {Request, Response} from "express";
import {ApiRequestObject, ApiRequests} from "../data_common/api_requests/api_request";
import {AxiosRequestConfig} from "axios";


export class RequestForwarding {

    static async forwardRequest(req: Request, res: Response,dataObject: DataObject, redirect: string): Promise<any>{

        const apiConfig: ApiRequestObject = ApiRequests.getApiConfig(dataObject, {
            application: null as unknown as string,
            version: null as unknown as number
        });

        const apiRequests = new ApiRequests(apiConfig);

        let url = redirect;

        if(req.params){
            for (const param in req.params){
                url += req.params[param] + '/';
            }
        }

        if(req.query){
            url+= '?';
            for (const query in req.query ) {
                url += query + '=' + req.query[query] + '&';
            }
        }

        const requestObject = {
            url,
            method: req.method
        } as AxiosRequestConfig;

        if(dataObject.data) {
            requestObject.data = dataObject.data;
        }

        return await apiRequests.doRequest(requestObject)
            .then((r: any) => {
                if(r.rs_error){
                    return res.status(500).json({
                        error: 'Unable to forward request',
                        description: r.rs_error
                    })
                }

                return res.json(r);

            })
            .catch((err: any) => {
                return res.status(500).json({
                    error: 'Unable to forward request',
                    description: err.rs_error
                })
            });
    }
}